import requests
import base64
import datetime


news_apikey = "6c82a718c61f4c9285cceeca24d4bc7f"

spotify_client_id = "e4ef79fb89434c77a0c0ee2ccefa0698"
spotify_client_secet = "b5a7d8992c4446fcb375b17b26bfab79"


def authorization_token():
    # Authorization
    url = "https://accounts.spotify.com/api/token"
    headers = {}
    data = {}

    # Encode as Base64
    message = f"{spotify_client_id}:{spotify_client_secet}"
    messageBytes = message.encode("ascii")
    base64Bytes = base64.b64encode(messageBytes)
    base64Message = base64Bytes.decode("ascii")

    headers["Authorization"] = f"Basic {base64Message}"
    data["grant_type"] = "client_credentials"

    response = requests.post(url, headers=headers, data=data)

    token = {
        "apikey": response.json()["access_token"],
        "expire_at": datetime.datetime.now()
        + datetime.timedelta(seconds=3000),
    }
    print("********** Spotify Token Received **********")
    return token
