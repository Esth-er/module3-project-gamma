from models import MusicOut, NewsOut
import requests
from datetime import datetime, date
import os


class MusicQueries:
    def get_artist(self, artist_name: str, token: str) -> MusicOut:
        params = {
            "q": artist_name,
            "type": "artist",
        }
        return requests.get(
            "https://api.spotify.com/v1/search",
            params=params,
            headers=token,
        ).json()

    def get_track(self, track_name: str, token: str) -> MusicOut:
        params = {
            "q": track_name,
            "type": "track",
        }

        return requests.get(
            "https://api.spotify.com/v1/search",
            params=params,
            headers=token,
        ).json()

    def get_album(self, album_name: str, token: str) -> MusicOut:
        params = {
            "q": album_name,
            "type": "album",
        }

        return requests.get(
            "https://api.spotify.com/v1/search",
            params=params,
            headers=token,
        ).json()

    def get_new_releases(self, token: str) -> MusicOut:
        params = {
            "country": "US",  # we could possibly add country filter ability, country list are pre-disclosed in country.py file
        }

        return requests.get(
            "https://api.spotify.com/v1/browse/new-releases",
            params=params,
            headers=token,
        ).json()

    def get_featured_playlists(self, token: str) -> MusicOut:
        params = {
            "country": "US",  # we could possibly add country filter ability, country list are pre-disclosed in country.py file
        }

        return requests.get(
            "https://api.spotify.com/v1/browse/featured-playlists",
            params=params,
            headers=token,
        ).json()

    def get_categories(self, token: str) -> MusicOut:
        params = {
            "country": "US",  # we could possibly add country filter ability, country list are pre-disclosed in country.py file
        }

        return requests.get(
            "https://api.spotify.com/v1/browse/categories",
            params=params,
            headers=token,
        ).json()

    def get_api_data(self, url_link: str, token: str) -> MusicOut:
        return requests.get(
            url=url_link,
            headers=token,
            timeout=30,
        ).json()

    def get_artist_news(self, artist_name: str, token: str) -> NewsOut:
        params = {
            "q": artist_name,
            "to": date.today(),
            "sortBy": "relevancy",
            "apiKey": os.environ["NEWS_API_KEY"],
            "language": "en",
        }
        return requests.get(
            "https://newsapi.org/v2/everything",
            params=params,
            headers=token,
        ).json()

    def get_music_news(self, token: str) -> NewsOut:
        params = {
            "q": "music",
            "to": date.today(),
            "sortBy": "relevancy",
            "apiKey": os.environ["NEWS_API_KEY"],
            "language": "en",
            "searchIn": "description",
        }
        return requests.get(
            "https://newsapi.org/v2/everything",
            params=params,
            headers=token,
        ).json()
