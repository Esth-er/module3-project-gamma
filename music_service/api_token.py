import requests
import base64
import datetime
import os


spotify_client_id = os.environ["SPOTIFY_CLIENT_KEY"]
spotify_client_secet = os.environ["SPOTIFY_CLIENT_SECRET_KEY"]


def authorization_token():
    # Authorization
    url = "https://accounts.spotify.com/api/token"
    headers = {}
    data = {}

    # Encode as Base64
    message = f"{spotify_client_id}:{spotify_client_secet}"
    messageBytes = message.encode("ascii")
    base64Bytes = base64.b64encode(messageBytes)
    base64Message = base64Bytes.decode("ascii")

    headers["Authorization"] = f"Basic {base64Message}"
    data["grant_type"] = "client_credentials"

    response = requests.post(url, headers=headers, data=data)

    token = {
        "apikey": response.json()["access_token"],
        "expire_at": datetime.datetime.now()
        + datetime.timedelta(seconds=3000),
    }
    print("********** Spotify Token Received **********")
    return token
