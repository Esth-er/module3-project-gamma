from fastapi import APIRouter, Depends
from api_token import authorization_token
import os
import requests
from queries.music_queries import MusicQueries
from datetime import datetime, date


token = authorization_token()
router = APIRouter()


def validate_token():
    global token
    if token["expire_at"] < datetime.now():
        token = authorization_token()

    return {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": "Bearer " + token["apikey"],
    }


@router.get("/api/search/artist/{artist_name}")
def get_artist(artist_name: str, repo: MusicQueries = Depends()):
    return repo.get_artist(artist_name=artist_name, token=validate_token())


@router.get("/api/search/track/{track_name}")
def get_track(track_name: str, repo: MusicQueries = Depends()):
    return repo.get_track(track_name=track_name, token=validate_token())


@router.get("/api/search/album/{album_name}")
def get_album(album_name: str, repo: MusicQueries = Depends()):
    return repo.get_album(album_name=album_name, token=validate_token())


@router.get("/api/browse/new-releases")
def get_new_releases(repo: MusicQueries = Depends()):
    return repo.get_new_releases(token=validate_token())


@router.get("/api/browse/featured-playlists")
def get_featured_playlists(repo: MusicQueries = Depends()):
    return repo.get_featured_playlists(token=validate_token())


# music categories that has [pop, hip-pop, top hits, latin] etc
@router.get("/api/browse/categories")
def get_categories(repo: MusicQueries = Depends()):
    return repo.get_categories(token=validate_token())


@router.post("/api/browse/fetch/")
def get_api_data(url_link: str, repo: MusicQueries = Depends()):
    return repo.get_api_data(url_link=url_link, token=validate_token())


@router.get("/api/news/{artist_name}")
def get_artist_news(artist_name: str, repo: MusicQueries = Depends()):
    return repo.get_artist_news(
        artist_name=artist_name, token=validate_token()
    )


@router.get("/api/news/music")
def get_music_news(repo: MusicQueries = Depends()):
    return repo.get_music_news(token=validate_token())
