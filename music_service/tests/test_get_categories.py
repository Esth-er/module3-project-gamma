from fastapi.testclient import TestClient
from main import app
from queries.music_queries import MusicQueries


client = TestClient(app)


token = "basjkhdbasjkdbyusafbvasjufvb21312412412"


class MusicQueriesMock:
    def get_categories(self, token):
        response = {"topic": {}}
        return response


# Wayne
def test_get_categories():
    # Arrange
    app.dependency_overrides[MusicQueries] = MusicQueriesMock
    app.dependency_overrides["token"] = token
    # Act
    response = client.get("/api/browse/categories")
    # Assert
    assert response.status_code == 200
    assert response.json() == {"topic": {}}
    # clean up
    app.dependency_overrides = {}
