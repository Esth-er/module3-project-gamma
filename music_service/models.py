from bson.objectid import ObjectId
from pydantic import BaseModel
from typing import List, Optional


class MusicOut(BaseModel):
    musicdata: dict


class NewsOut(BaseModel):
    newsdata: dict
