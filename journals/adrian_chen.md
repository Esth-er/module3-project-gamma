## January 26, 2023
Final sprint. Cleaned up a lot of the formatting issues. Added more information on each of the three search results —- artist, track, album, including the links to the official spotify page and preview.


The formats of each of the three fetched categories are so different that I almost gave up on trying to display all three types of results. I am glad that I kept on trying.


Also added small features to the music page/new release page.


## January 25, 2023
Tried to fix the bug of my search.js where the page was not properly displaying the data I fetched from the spotify api.


Turned out I needed an if statement for when the result state is equal to null to display the search bar also.


## January 24, 2023
Jerrika had some merge conflicts where she said she lost progress after I merged to main. Spent a lot of time with the team talking about and trying to figure out that issue.


Started working on the ReadMe documentation.


Continue working on the search page — I spent a lot of time of the day on the bug where I need to click twice to render the page.




## January 23, 2023
Spent most of the day writing the music page, which fetches the new released music from the spotify api and displays them.


The page looks very nice with beautiful album covers.


Also spent some time debugging with my teammates. Fixed the nav bar and cleaned up the code a bit.


## January 20, 2023
Spent a lot of time with Esther fixing the token issue – it did not show up in cookies and returns false – on the signup and login page.


## January 19, 2023
I kept getting the token expiration issue on the search page. Spent a lot of time with the seirs trying to debug. Turned out that there was a tiny typo, and the localhost should have been 8100 instead of 8000.


## January 18, 2023
Continued to work on the search page. Created a search bar which includes three categories to choose from – artist, track and album – all in one place. It was new and difficult to me so I spent a lot of time figuring the dropdown option out.


Right now the skeleton of the search page is here, but I still have some issues successfully fetching the data.


## January 17, 2023
Started on the search page. Did research on Redux and React. I created a Redux folder and created one of the toolkits.


Also created the forms for the playlist, where you can create and modify a playlist for yourself.


## January 16, 2023
Spent some time over the break thinking about what to do with the front end portion of our project. Discussing with teams whether to go with redux and flesh out some of the functionalities that we want to go for.


## January 13, 2023
I deleted my old branch and started a new one. Also had to refork my project and clone them onto my computer. Finally fixed the issue and the ghi can start now. Unfortunately I lost the history on my old branch.


## January 12, 2023
Had an issue with my containers after I merged from main. Couldn’t start one of the containers – the ghi always quit after I docker compose up. Spent a lot of time trying to figure out why.


Had some help from Esther.


## January 11, 2023
Worked with Esther on the login page on the formatting issue.


## January 10, 2023
I did research on how to connect the mongodb database to our project. Spent some time with Wayne talking about this.


## January 9, 2023
Continued to figure out how to make the spotify api work for our project – properly format the headers to meet their requirement.


## January 6, 2023
Did research on the spotify web api, trying to figure out how to match the routes and make them work in our application.


## January 5, 2023
Studied fastAPI and tried to figure out how the routes work for our project. Brainstormed with teammates.


## January 4, 2023
Did some housekeeping and worked on project setup. Forked the repo and created my own branch. Did some testing on how to set up the project.


## December 22, 2022
Continued to work on the wireframing and flesh out some ideas.


## December 21, 2022
Working with my teammates on the wireframing for our project. We did a lot of Brainstorming and came up with a lot of ideas about the functionalities for beatbuzz. For example, one major feature that we want to go for is the functionality of presenting the top songs from different countries for the viewers. I think it would be an exciting way for people to discover new music from all around the world.


## December 20, 2022
I talked about my ideas for the project with my teammate. Spent a lot of time deciding. Created some pages on excalidraw.
