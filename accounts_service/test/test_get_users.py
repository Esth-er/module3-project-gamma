from fastapi.testclient import TestClient
from queries.accounts_queries import AccountQueries

from main import app

client = TestClient(app)


class AccountQueriesMock:
    def get_all(self):
        return [
            {"id": "123", "email": "test@test.com", "full_name": "jane doe"},
            {"id": "143", "email": "test1@test1.com", "full_name": "john doe"},
        ]


def test_account():
    app.dependency_overrides[AccountQueries] = AccountQueriesMock

    response = client.get("/api/accounts/")

    assert response.status_code == 200
    assert len(response.json()) > 1

    app.dependacy_overrides = {}


class AccountQueriesCreateMock:
    created_account = None
    def create_account(self, account_data):
        self.created_account = account_data
        return True

def test_create_account():
    account_queries_mock = AccountQueriesCreateMock()
    app.dependency_overrides[AccountQueries] = account_queries_mock

    account_data = {
        "email": "test1@test1.com",
        "password": "password",
        "full_name": "john doe",
    }

    response = client.post("/api/accounts", json=account_data)
    assert response.status_code == 307

    app.dependency_overrides = {}
