from models import AccountIn, AccountOut, Account, AccountUpdateIn
from .client import Queries
from pymongo.errors import DuplicateKeyError
from pymongo import ReturnDocument
from bson.objectid import ObjectId
from typing import Union


class DuplicateAccountError(ValueError):
    pass


class AccountQueries(Queries):
    DB_NAME = "library"
    COLLECTION = "accounts"

    def get(self, email: str) -> Account:
        props = self.collection.find_one({"email": email})
        if not props:
            return None
        props["id"] = str(props["_id"])
        return Account(**props)

    def get_by_id(self, id: str) -> Account:
        props = self.collection.find_one({"_id": ObjectId(id)})
        if not props:
            return None
        props["id"] = str(props["_id"])
        return Account(**props)

    def create(self, info: AccountIn, hashed_password: str) -> Account:
        props = info.dict()
        props["password"] = hashed_password
        if self.collection.find_one({"email": props["email"]}):
            raise DuplicateAccountError()
        self.collection.insert_one(props)
        props["id"] = str(props["_id"])
        return Account(**props)

    def get_all(self) -> list[AccountOut]:
        db = self.collection.find()
        account_emails = []
        for document in db:
            document["id"] = str(document["_id"])
            account_emails.append(AccountOut(**document))
        return account_emails

    def update(
        self,
        id: str,
        info: AccountUpdateIn,
        hashed_password: Union[None, str],
    ):
        props = info.dict()
        if not props["password"]:
            props["password"] = hashed_password
        try:
            self.collection.find_one_and_update(
                {"_id": ObjectId(id)},
                {"$set": props},
                return_document=ReturnDocument.AFTER,
            )
        except LookupError:
            raise DuplicateAccountError()
        return Account(**props, id=id)

    def delete(self, id: str) -> bool:
        return (
            True
            if self.collection.delete_one({"_id": ObjectId(id)})
            else False
        )
