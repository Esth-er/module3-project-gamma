from models import PlayListsIn, PlayListOut, PlayListUpdate
from .client import Queries
from pymongo.errors import DuplicateKeyError
from pymongo import ReturnDocument
from bson.objectid import ObjectId
from typing import Union


class DuplicatePlaylistError(ValueError):
    pass


class PlaylistQueries(Queries):
    DB_NAME = "library"
    COLLECTION = "playlists"

    def get(self, email: str) -> PlayListOut:
        props = self.collection.find_one({"email": email})
        if not props:
            return None
        return PlayListOut(**props)

    def create(self, info: PlayListsIn) -> PlayListOut:
        props = info.dict()
        if self.collection.find_one({"playlist_name": props["playlist_name"]}):
            raise DuplicatePlaylistError()
        self.collection.insert_one(props)
        return PlayListOut(**props)

    def get_all(self) -> list[PlayListOut]:
        db = self.collection.find()
        playlists = []
        for document in db:
            document["email"] = str(document["email"])
            playlists.append(PlayListOut(**document))
        return playlists

    def update(
        self,
        email: str,
        playlist_name: str,
        info: PlayListUpdate,
    ):
        props = info.dict()
        try:
            self.collection.find_one_and_update(
                {"email": email, "playlist_name": playlist_name},
                {"$set": props},
                return_document=ReturnDocument.AFTER,
            )
        except LookupError:
            raise DuplicatePlaylistError()
        return PlayListOut(**props)

    def delete(self, email: str, playlist_name: str) -> bool:
        db = self.collection.delete_one(
            {"email": email, "playlist_name": playlist_name}
        )
        return True if db.deleted_count > 0 else False
