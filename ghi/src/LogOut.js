import { useToken } from './UseToken';
import { useNavigate } from 'react-router-dom';

function Logout(){
    const [token, , logout] = useToken();
    const navigate = useNavigate();
    logout(token);
}

export default Logout;
