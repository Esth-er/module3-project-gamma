import { useState, useEffect } from 'react'

function SearchView() {
  const dropdownOptions = ['category','artist','track','album']
  const [dropdownSelection, setDropdownSelection] = useState(dropdownOptions[0])
  const [searchSelection, setSearchSelection] = useState("")
  const [result, setResult] = useState(null)


  async function handleSubmit() {
    let url
    if (dropdownSelection === 'artist') {
        url = `${process.env.REACT_APP_BEATBUZZ_MUSIC_API_HOST}/api/search/artist/${searchSelection}`
        const response = await fetch(url)
        const data = await response.json()
        console.log("look:3", data)
        const data1 = data.artists.items[0]
        console.log("look:artist", data1)
        setResult(data1)
    }
    else if (dropdownSelection === 'track') {
        url = `${process.env.REACT_APP_BEATBUZZ_MUSIC_API_HOST}/api/search/track/${searchSelection}`
        const response = await fetch(url)
        const data = await response.json()
        console.log("look:3", data)
        const data2 = data.tracks.items[0]
        console.log("look:track", data2)
        setResult(data2)
    }
    else {
        url = `${process.env.REACT_APP_BEATBUZZ_MUSIC_API_HOST}/api/search/album/${searchSelection}`
        const response = await fetch(url)
        const data = await response.json()
        console.log("look:3", data)
        const data3 = data.albums.items[0]
        console.log("look:album", data3)
        setResult(data3)
    }

  }


  if (result === null) {
    return (<div className="app">
            <h2>Search Your Favourite Music</h2>
            <div className="dropdown-container">

              <div className="form-outline">
                      <input onChange ={e=>setSearchSelection(e.target.value)} value={searchSelection} type="text" id="form1" className="form-control" />
              </div>

              <select name="select" id="select" onChange={e => setDropdownSelection(e.target.value)}>
                  {
                    dropdownOptions.map(option => <option value={option} key={option} >{option}</option>)
                  }
              </select>

              <button type="button" className="btn btn-primary" onClick={handleSubmit}>
                  <p>SEARCH BEATBUZZ</p>
                  <i className="fas fa-search"></i>
              </button>

            </div>
      </div>)


  }

  else if (dropdownSelection === "artist") {
    return (

    <div>
      <div className="app">
            <h2>Search Your Favourite Music</h2>
            <div className="dropdown-container">

              <div className="form-outline">
                      <input onChange ={e=>setSearchSelection(e.target.value)} value={searchSelection} type="text" id="form1" className="form-control" />
              </div>

              <select name="select" id="select" onChange={e => setDropdownSelection(e.target.value)}>
                  {
                    dropdownOptions.map(option => <option value={option} key={option}>{option}</option>)
                  }
              </select>

              <button type="button" className="btn btn-primary" onClick={handleSubmit}>
                  <p>SEARCH BEATBUZZ</p>
                  <i className="fas fa-search"></i>
              </button>

            </div>
      </div>

      <div className="container text-center">
          <h2 className="text-center">Artist Research Result</h2>
          <div className="row align-items-start">
            <div className="col mb-4" key="id">
                  <div className="card" style={{ height: "32rem", width: "21.1rem" }}>
                    <img src={result.images[0].url} className="card-img-top" alt={result.genres} style={{ height: "19.5rem", width: "21rem" }} />
                    <div className="card-body">
                      <h6 className="card-title">
                        <a href={result.external_urls.spotify} target="_blank" rel="noopener noreferrer">
                        <b>{result.name}</b>
                        </a>
                      </h6>
                      <p></p>
                      <p></p>
                      <p className="card-text">Followers: {result.followers.total}</p>
                      <p className="card-text">Genre: {result.genres[0]}</p>
                    </div>
                  </div>
                </div>
          </div>
        </div>

  </div>

  )
  }

  else if (dropdownSelection === "track") {
    return (

    <div>
      <div className="app">
            <h2>Search Your Favourite Music</h2>
            <div className="dropdown-container">

              <div className="form-outline">
                      <input onChange ={e=>setSearchSelection(e.target.value)} value={searchSelection} type="text" id="form1" className="form-control" />
              </div>

              <select name="select" id="select" onChange={e => setDropdownSelection(e.target.value)}>
                  {
                    dropdownOptions.map(option => <option value={option} key={option}>{option}</option>)
                  }
              </select>

              <button type="button" className="btn btn-primary" onClick={handleSubmit}>
                  <p>SEARCH BEATBUZZ</p>
                  <i className="fas fa-search"></i>
              </button>

            </div>
      </div>

      <div className="container text-center">
          <h2 className="text-center">Track Research Result</h2>
          <div className="row align-items-start">
            <div className="col mb-4" key="id">
                  <div className="card" style={{ height: "35rem", width: "21.1rem" }}>
                    <img src={result.album.images[0].url} className="card-img-top" alt={result.genres} style={{ height: "19.5rem", width: "21rem" }} />
                    <div className="card-body">
                      <h6 className="card-title">
                        <a href={result.external_urls.spotify} target="_blank" rel="noopener noreferrer">
                        <b>{result.name}</b>
                        </a>
                      </h6>
                      <p className="card-text">Artist: {result.artists[0].name}</p>
                      <p className="card-text">Album: {result.album.name}</p>
                      <p className="card-text">{result.album.release_date}</p>
                      <a href={result.preview_url} target="_blank" rel="noopener noreferrer">
                        <b>Preview~</b>
                      </a>
                    </div>
                  </div>
                </div>
          </div>
        </div>

  </div>

  )
  }

  return (

    <div>
      <div className="app">
            <h2>Search Your Favourite Music</h2>
            <div className="dropdown-container">

              <div className="form-outline">
                      <input onChange ={e=>setSearchSelection(e.target.value)} value={searchSelection} type="text" id="form1" className="form-control" />
              </div>

              <select name="select" id="select" onChange={e => setDropdownSelection(e.target.value)}>
                  {
                    dropdownOptions.map(option => <option value={option} key={option}>{option}</option>)
                  }
              </select>

              <button type="button" className="btn btn-primary" onClick={handleSubmit}>
                  <p>SEARCH BEATBUZZ</p>
                  <i className="fas fa-search"></i>
              </button>

            </div>
      </div>

      <div className="container text-center" >
          <h2 className="text-center">Album Research Result</h2>
          <div className="row align-items-start">
            <div className="col mb-4" key="id">
                  <div className="card" style={{ height: "33rem", width: "21.1rem" }}>
                    <img src={result.images[0].url} className="card-img-top" alt={result.genres} style={{ height: "19.5rem", width: "21rem" }} />
                    <div className="card-body">
                      <h6 className="card-title">
                        <a href={result.external_urls.spotify} target="_blank" rel="noopener noreferrer">
                        <b>Album: {result.name}</b>
                        </a>
                      </h6>
                      <p className="card-text">Artist: {result.artists[0].name}</p>
                      <p className="card-text">{result.release_date}</p>
                      <p className="card-text">Total Tracks: {result.total_tracks}</p>
                    </div>
                  </div>
                </div>
          </div>
        </div>

  </div>

  )

}

export default SearchView
