import React, { useState } from 'react';
import { NavLink, useNavigate } from "react-router-dom";
import ReactDOM from 'react-dom';
import { useToken } from "../UseToken";
import '../App.css';

function BootstrapInput(props) {
    const { id, placeholder, labelText, value, onChange, type } = props;

    return (
        <div className="mb-3">
            <label htmlFor={id} className="form-label">{labelText}</label>
            <input value={value} onChange={onChange} required type={type} className="form-control" id={id} placeholder={placeholder}/>
        </div>
    )
}

function AccountForm(props) {
    const [email, setEmail] = useState('');
    const [name, setFullName] = useState('');
    const [password, setPassword] = useState('');
    const [token, signup] = useToken();
    const navigate = useNavigate();

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            const url = `${process.env.REACT_APP_BEATBUZZ_API_HOST}/api/accounts/`;
            const response = await fetch(url, {
                method: 'POST',
                body: JSON.stringify({
                "email": email,
                "full_name": name,
                "password":  password,
                }),
                headers: {
                    "Content-Type": "application/json",
                },
            });
    if (response.ok) {
      await signup(email, password);
      setEmail('')
      setFullName('')
      setPassword('')
      console.log(token)
      // localStorage.setItem(token, signup);
      navigate('/')
    }
    // return false;

    } catch (error) {
      console.log(error);
      console.log("Wrong email or password!");
    }
  }

    return (
        <>
        <form onSubmit={handleSubmit} id="create-account-form">
            <BootstrapInput
                id="email"
                placeholder="you@example.com"
                labelText="Your email address"
                value={email}
                onChange={e => setEmail(e.target.value)}
                type="email" />
            <BootstrapInput
                id="name"
                placeholder="Your name"
                labelText="Your name"
                value={name}
                onChange={e => setFullName(e.target.value)}
                type="text" />
            <BootstrapInput
                id="password"
                placeholder="Super secret password"
                labelText="Password"
                value={password}
                onChange={e => setPassword(e.target.value)}
                type="password" />
            <button type="submit" className="btn btn-primary">SUBMIT</button>
        </form>
        </>
    );
}

export default AccountForm;
