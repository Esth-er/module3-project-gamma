import React, { useState, useEffect } from 'react';
import Trend from 'react-trend';
import gsap from 'gsap';
import './App.css';
// import MagicText from './MagicText';

const MainPage = () => {
  useEffect(() => {
    document.body.addEventListener("mousemove", evt => {
      const mouseX = evt.clientX;
      const mouseY = evt.clientY;

      gsap.set(".cursor", {
        x: mouseX,
        y: mouseY,
      });

      gsap.to(".shape", {
        x: mouseX,
        y: mouseY,
        stagger: -0.1
      });
       return () => {
      document.body.removeEventListener("mousemove", evt => {});
    }
    });

  }, []);
  return (

    <div>
      <div className="py-12 px-8 md:px-24 flex flex-col items-center">
    <div className="cursor"></div>
    <div className="shapes">
    <div className="shape shape-1"></div>
    <div className="shape shape-2"></div>
    <div className="shape shape-3"></div>
  </div>
  </div>

        <div className="content">
        <h1 className="title">BeatBuzz</h1>


        <Trend
            className="trend"
            smooth
            autoDraw
            autoDrawDuration={3000}
            autoDrawEasing="ease-out"
            data={[0,2,5,9,5,10,3,5,0,0,1,8,2,9,0]}
            gradient={['#673ab7', '#9c27b0', '#e91e63']}
            radius={10}
            strokeWidth={2}
            strokeLinecap={'butt'}
          />



  </div>
  </div>

  );
}




export default MainPage;
