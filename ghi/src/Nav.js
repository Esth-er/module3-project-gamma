import React from "react";
import { createRef } from "react";
import gsap from "gsap";
import "./App.css";

const { useRef, useState, useEffect } = React;

function Nav() {
  const items = [
    {
      name: "Home",
      color: "#f44336",
      href: "/",
    },
    {
      name: "Explore",
      color: "#e91e63",
      href: "/explorepage",
    },
    {
      name: "Music",
      color: "#9c27b0",
      href: "/music",
    },
    // {
    //   name: "Playlists",
    //   color: "#673ab7",
    //   href: "/account/playlists",
    // },
    {
      name: "Search",
      color: "#673ab7",
      href: "/search",
    },
    // {
    //   name: "Account",
    //   color: "#3f51b5",
    //   href: "/account/profile",
    // },

    // {
    //   name: "LogIn",
    //   color: "#e91e63",
    //   href: "/account/login",
    // },
    // {
    //   name: "LogOut",
    //   color: "#f44336",
    //   href: "/account/logout",
    // },
  ];

  const $root = createRef();
  const $indicator1 = createRef();
  const $indicator2 = createRef();
  const $items = items.map(createRef);
  const [active, setActive] = React.useState(0);

  const animate = () => {
    const menuOffset = $root.current.getBoundingClientRect();
    const activeItem = $items[active].current;
    const { width, height, top, left } = activeItem.getBoundingClientRect();

    const settings = {
      x: left - menuOffset.x,
      y: top - menuOffset.y,
      width: width,
      height: height,
      backgroundColor: items[active].color,
      ease: "elastic.out(.7, .7)",
      duration: 0.8,
    };

    gsap.to($indicator1.current, {
      ...settings,
    });

    gsap.to($indicator2.current, {
      ...settings,
      duration: 1,
    });
  };

  React.useEffect(() => {
    animate();
    window.addEventListener("resize", animate);

    return () => {
      window.removeEventListener("resize", animate);
    };
  }, [active]);

  return (
    <div ref={$root} className="menu">
      {items.map((item, index) => (
        <a
          key={item.name}
          ref={$items[index]}
          className={`item ${active === index ? "active" : ""}`}
          onMouseEnter={() => {
            setActive(index);
          }}
          href={item.href}
        >
          {item.name}
        </a>
      ))}
      <div ref={$indicator1} className="indicator" />
      <div ref={$indicator2} className="indicator" />
    </div>
  );
}

export default Nav;

//   { return (
//     <nav className="navbar bg-body-tertiary">
//       <div className="container-fluid">
//         <NavLink className="navbar-brand" to="/">
//           <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Work_With_Sounds_LOGO.jpg/640px-Work_With_Sounds_LOGO.jpg" alt="Logo" width="30" height="24" className="d-inline-block align-text-top"></img>
//           Music
//         </NavLink>

//         <li className="nav-item dropdown">
//                 <a className="nav-link dropdown-toggle" href="http://localhost:8100/api/" role="button" data-bs-toggle="dropdown" aria-expanded="false">
//                   Account
//                 </a>
//                 <ul className="dropdown-menu">
//                   <li><NavLink className="nav-link" aria-current="page" to="/account/create/">Create Account</NavLink></li>
//                 </ul>
//         </li>

//         <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
//           <span className="navbar-toggler-icon"></span>
//         </button>
//         <div className="collapse navbar-collapse" id="navbarSupportedContent">
//           <ul className="navbar-nav me-auto mb-2 mb-lg-0">
//           </ul>
//         </div>
//       </div>
//     </nav>
//   )
// }
