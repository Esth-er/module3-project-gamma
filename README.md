## BeatBuzz

Adrian Chen

Esther Lee

Jerrika Jentsch

Wayne Zheng

BeatBuzz -- an all in one music hub


## Design

- [API design](docs/api-design.md)
- [GHI](docs/WireFrame.excalidraw)


## Intended market

We are targeting music listeners who are interested in venturing beyond their comfort zone and being exposed to music and music related news from all over the world.

## Functionality

Visitors to the site can sign up for new profiles, log in to their music profiles and log out their profiles.

Users can click on the explore tab in the navigation bar to explore carefully curated playlists, which have a wide range of themes. They can also access the songs inside of the playlist by clicking on the “Show Playlist” button. Clicking on the individual songs will redirect users to the spotify detail page.

Music page for a list view of new released music with amazing artwork.

The playlist page where visitors can create and modify their own playlists after creating their profiles. (stretch goals)

Search page.
1. Visitors can search by three different categories – artist, track and album.
2. Each selection of categories will return different formats of search results, which includes the information of the artwork and links to the official spotify page.
3. Visitors can preview the songs in the “track” search result.

Account page with detailed presentation of account information. (strech goals)

## Project Initialization

To fully enjoy this application on your local machine, please make sure to follow these steps:
Clone the repository down to your local machine
CD into the new project directory
Run docker volume create mongodb
Run docker compose build
Run docker compose up
