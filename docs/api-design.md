# USER ENDPOINTS

### Fetch Current User Token Status

- Endpoint Path: /token
- Endpoint Method: GET

- Headers:

  - accept: application/json
  - Authorization: Bearer token

- Response: User's info(user id, email, fullname, with access token)
- Response shape (JSON):
  ```json
  {
    "access_token": "string",
    "token_type": "Bearer",
    "account": {
      "id": "string",
      "email": "string",
      "full_name": "string"
    }
  }
  ```

### Login

- Endpoint Path: /token
- Endpoint Method: POST

- Headers:

  - accept: application/json

- Request body:
  ```json
  {
    "username": "string",
    "password": "string"
  }
  ```
- Response: A Token if user are successfully authorized
- Response shape (JSON):
  ```json
  {
    "access_token": "string",
    "token_type": "Bearer"
  }
  ```

### LogOut

- Endpoint Path: /token
- Endpoint Method: DELETE

- Headers:

  - accept: application/json
  - Authorization: Bearer token

- Response: logout current user
- Response shape (JSON):
  ```json
  {
    "logout": "Boolean"
  }
  ```

### Protected endpoint (check user login status)

- Endpoint Path: /api/protected
- Endpoint Method: GET

- Headers:

  - accept: application/json
  - Authorization: Bearer token

- Response: Boolean if user are logged in or not
- Response shape (JSON):
  ```json
  {
    "Status": "Boolean"
  }
  ```

### Accounts endpoint (List of User Accounts) //database checking purpose, disable prior to product launch

- Endpoint Path: /api/accounts
- Endpoint Method: GET

- Headers:

  - accept: application/json

- Response: List of Accounts registered
- Response shape (JSON):
  ```json
  [
    {
      "id": "string",
      "email": "string",
      "full_name": "string"
    }
  ]
  ```

### Create Account

- Endpoint Path: /api/accounts
- Endpoint Method: POST

- Headers:

  - accept: application/json

- Request body(JSON):

  ```json
  {
    "email": "string",
    "password": "string",
    "full_name": "string"
  }
  ```

- Response: User's info(user id, email, fullname, with access token)
- Response shape (JSON):
  ```json
  {
    "access_token": "string",
    "token_type": "Bearer",
    "account": {
      "id": "string",
      "email": "string",
      "full_name": "string"
    }
  }
  ```

### Check Account

- Endpoint Path: /api/accounts/{account_email}
- Endpoint Method: GET

- Headers:

  - accept: application/json

- Query parameters:
  -q:[
  account_email,
  ]
- Response: User's info(user id, email, fullname)
- Response shape (JSON):
  ```json
  {
    "id": "string",
    "email": "string",
    "full_name": "string"
  }
  ```

### Update Account

- Endpoint Path: /api/accounts/{account_id}
- Endpoint Method: PUT

- Headers:

  - accept: application/json

- Query parameters:
  -q:[
  account_id,
  ]

- Request body(JSON):

  ```json
  {
    "email": "string/OPTIONAL",
    "password": "string/OPTIONAL",
    "full_name": "string/OPTIONAL"
  }
  ```

- Response: User's info(email, password, fullname)
- Response shape (JSON):
  ```json
  {
    "email": "string",
    "password": "string",
    "full_name": "string"
  }
  ```

### DELETE Account

- Endpoint Path: /api/accounts/{account_id}
- Endpoint Method: DELETE

- Headers:

  - accept: application/json

- Query parameters:
  -q:[
  account_id,
  ]

- Response: Boolean if its deleted
- Response shape (JSON):
  ```json
  {
    "status": "Boolean"
  }
  ```

# MUSIC API ENDPOINTS

### Search Artist

- Endpoint Path: /api/search/artist/{artist_name}
- Endpoint Method: GET

- Headers:

  - accept: application/json

- Query parameters:
  -q:[
  artist_name,
  ]

- Response: Artist Detail
- Response shape (JSON):
  ```json
  {
    "Artists": {
      "href": "string",
      "items": [
        {
          "artist": "string",
          "image": "string"
        }
      ]
    }
  }
  ```

### Search Track

- Endpoint Path: /api/search/artist/{track_name}
- Endpoint Method: GET

- Headers:

  - accept: application/json

- Query parameters:
  -q:[
  track_name,
  ]

- Response: Track Detail
- Response shape (JSON):
  ```json
  {
    "track": {
      "href": "string",
      "items": [
        {
          "track": "string",
          "image": "string"
        }
      ]
    }
  }
  ```

### Search Album

- Endpoint Path: /api/search/artist/{album_name}
- Endpoint Method: GET

- Headers:

  - accept: application/json

- Query parameters:
  -q:[
  album_name,
  ]

- Response: Album Detail
- Response shape (JSON):
  ```json
  {
    "album": {
      "href": "string",
      "items": [
        {
          "album": "string",
          "image": "string"
        }
      ]
    }
  }
  ```

### New releases

- Endpoint Path: /api/browse/new-releases
- Endpoint Method: GET
- Headers:

  - accept: application/json

- Response: List of new releases
- Response shape (JSON):
  ```json
  {
    "new releases": {
      "href": "string",
      "items": [
        {
          "album": "string",
          "image": "string"
        }
      ]
    }
  }
  ```

### Featured Playlists

- Endpoint Path: /api/browse/featured-playlists
- Endpoint Method: GET
- Headers:

  - accept: application/json

- Response: List of featured playlists
- Response shape (JSON):
  ```json
  {
    "featured playlists": {
      "href": "string",
      "items": [
        {
          "playlists": "string",
          "image": "string"
        }
      ]
    }
  }
  ```

### Categories Lists

- Endpoint Path: /api/browse/categories
- Endpoint Method: GET
- Headers:

  - accept: application/json

- Response: List of categories
- Response shape (JSON):
  ```json
  {
    "Categories": {
      "href": "string",
      "items": [
        {
          "category": "string",
          "image": "string"
        }
      ]
    }
  }
  ```

### Fetch spotify linked data

- Endpoint Path: /api/browse/fetch/
- Endpoint Method: POST
- Headers:

  - accept: application/json

- Query parameters:
  -q:[
  url_link,
  ]
- Response: linked url's response
- Response shape (JSON):
  ```json
  {
    "response": {
      "data": {}
    }
  }
  ```

### Search Artist news

- Endpoint Path: /api/news/{artist_name}
- Endpoint Method: GET
- Headers:

  - accept: application/json

- Query parameters:
  -q:[
  artist_name,
  ]
- Response: Artist's recent news
- Response shape (JSON):
  ```json
  {
    "response": [
      {
        "news": {}
      }
    ]
  }
  ```

### Get recent music news

- Endpoint Path: /api/news/music
- Endpoint Method: GET
- Headers:

  - accept: application/json

- Response: Recent music news
- Response shape (JSON):
  ```json
  {
    "response": [
      {
        "news": {}
      }
    ]
  }
  ```

### Get Launch Detail

- Endpoint Path: /api/launch-details
- Endpoint Method: GET
- Headers:

  - accept: application/json

- Response: Our Launch Detail
- Response shape (JSON):
  ```json
  {
    "launch_details": {
      "year": "int",
      "month": "int",
      "day": "string",
      "hour": "int",
      "min": "int",
      "timezone:": "string"
    }
  }
  ```
